---
title: "Robert Wu (吳才銓)"
email: "rupert[at]{cs[dot]toronto[dot]edu, together[dot]ai}"
pages: "[about] [[projects]](projects/index.html) [[cv]](cv.pdf)"
links: "{ \
  [github](https://github.com/rhubarbwu), \
  [gitlab](https://gitlab.com/rhubarbwu), \
  [linkedin](https://www.linkedin.com/in/wu-robert/), \
  [scholar](https://scholar.google.com/citations?user=X0M3q_sAAAAJ&hl=en), \
  }"
---

## About Me

> I'm a junior researcher and recent MSc graduate from the **[University of Toronto (UofT)](https://cs.toronto.edu)/[Vector Institute](https://vectorinstitute.ai/)** advised by **[Prof. Vardan Papyan](https://sites.google.com/view/vardan-papyan/home)**. I am generally interested in deep learning and computer systems. I completed my BSc also at UofT ([Victoria College](https://vic.utoronto.ca)). I will be joining [together.ai](https://www.together.ai/) as a Researcher in January 2025.

---

## Publications \& Pre-Prints | [[all]](projects)

1. **Linguistic Collapse: Neural Collapse in (Large) Language Models**<br/>
   <u>Robert Wu</u>, [Vardan Papyan](https://sites.google.com/view/vardan-papyan)<br/>
   _NeurIPS 2024_ <br/>
   [**\[proceedings\]**](https://papers.nips.cc/paper_files/paper/2024/hash/f88cc8930b47a45ec4733123bf3039b9-Abstract-Conference.html)
   [**\[arxiv\]**](https://arxiv.org/abs/2405.17767)
   [**\[code\]**](https://github.com/rhubarbwu/linguistic-collapse)
2. **Towards One Shot Search Space Poisoning in Neural Architecture Search**<br/>
   [Nayan Saxena](https://uoft.me/nayan), <u>Robert Wu</u>, Rohan Jain<br/>
   _AAAI 2022 (Student Abstract+Poster)_<br/>
   [**\[proceedings\]**](https://ojs.aaai.org/index.php/AAAI/article/view/21658)
   [**\[poster\]**](https://www.cs.toronto.edu/~rupert/projects/oneshot-poster.pdf)
   [**\[arxiv\]**](https://arxiv.org/abs/2111.07138)
   [**\[code\]**](https://github.com/rhubarbwu/ENAS-Experiments)
3. **NeuralArTS: Structuring Neural Architecture Search with Type Theory**<br/>
   <u>Robert Wu</u>, [Nayan Saxena](https://uoft.me/nayan), Rohan Jain<br/>
   _AAAI 2022 (Student Abstract+Poster)_ <strong class=warning>(top 20, oral)</strong><br/>
   [**\[proceedings\]**](https://ojs.aaai.org/index.php/AAAI/article/view/21679)
   [**\[poster\]**](https://www.cs.toronto.edu/~rupert/projects/neuralarts-poster.pdf)
   [**\[arxiv\]**](https://arxiv.org/abs/2110.08710)
   [**\[code\]**](https://github.com/rhubarbwu/ENAS-Experiments)
4. **Poisoning the Search Space in Neural Architecture Search**<br/>
   <u>Robert Wu\*</u>, [Nayan Saxena](https://uoft.me/nayan)\*, Rohan Jain\*<br/>
   _ICML 2021 (AdvML Workshop)_<br/>
   [**\[openreview\]**](https://openreview.net/forum?id=fB3z4GrHCYv)
   [**\[poster\]**](https://icml.cc/media/PosterPDFs/ICML%202021/d67d8ab4f4c10bf22aa353e27879133c_Q3G0umv.png)
   [**\[arxiv\]**](https://arxiv.org/abs/2106.14406)
   [**\[code\]**](https://github.com/rhubarbwu/ENAS-Experiments)

(\* equal contribution)

---

## Teaching

- [CSC413H1](https://artsci.calendar.utoronto.ca/course/csc413h1): Neural Networks \& Deep Learning _(Winter 2024)_ with [Amir-massoud Farahmand](https://academic.sologen.net/) and Amanjit Singh Kainth
- [CSC207H1](https://artsci.calendar.utoronto.ca/course/csc207h1): Software Design _(Summer 2023)_
- [CSC413H5](https://utm.calendar.utoronto.ca/course/csc413h5): Neural Networks & Deep Learning _(Winter 2023)_ with [Florian Shkurti](http://www.cs.toronto.edu/~florian/)
- [CSC209H5](https://utm.calendar.utoronto.ca/course/csc209h5): Software Tools & Systems Prog. _(Winter 2023)_ with [Arnold Rosenbloom](http://www.cs.toronto.edu/~arnold/) and [Bahar Aameri](https://www.cs.toronto.edu/~bahar/)
- [CSC207H5](https://utm.calendar.utoronto.ca/course/csc207h5): Software Design _(Autumn 2022)_ with [Sonya Allin](https://www.teach.cs.toronto.edu/~sonyaa/)

<details>
<summary>As a Teaching Assistant</summary>

- [CSC413H5](https://utm.calendar.utoronto.ca/course/csc413h5)/[2516H1](https://github.com/craffel/csc2516-deep-learning-fall-2023): Neural Networks & Deep Learning _([Winter 2022](https://utm.calendar.utoronto.ca/course/csc413h5), [Autumn 2023](https://github.com/craffel/csc2516-deep-learning-fall-2023))_
- [CSC412/2506H1](https://artsci.calendar.utoronto.ca/course/csc412h1): Probabilistic Learning & Reasoning _(Winter 2022)_
- [CSC311H5](https://utm.calendar.utoronto.ca/course/csc311h5): Introduction to Machine Learning _(Autumn 2021)_
- [CSC165H1](https://artsci.calendar.utoronto.ca/course/csc165h1): [Mathematics] for Computer Science _(Winter 2020, Winter 2021)_
- [CSC/MATA67H3](https://utsc.calendar.utoronto.ca/course/csca67h3): Discrete Mathematics _(Autumn 2019)_
- [CSCA08H3](https://utsc.calendar.utoronto.ca/course/csca08h3): Introduction to Computer Science I _(Autumn 2018)_

</details>
