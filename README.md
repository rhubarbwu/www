## Styles

Webpage colour style sheets can be found in `static/*.css`. To apply one `<style>` when building the site,

```sh
sh build.sh <style>
```

## PDFs & Non-Text Resources

Files like images and PDFs are excluded from the `git` history as they're not critical to the webpage, can introduce unnecessary changes, and take up space. You can still upload such artifacts by placing them somewhere under `static/`, and use the scripts with flags.

```sh
sh build.sh <style> -pdf
sh deploy.sh -f
```

Alternatively, you can upload them manually using `scp` or remotely using `wget`. It is your responsibility to make sure the links point to the correct resources.
